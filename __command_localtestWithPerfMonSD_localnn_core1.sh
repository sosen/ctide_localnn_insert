#!/bin/bash
source setup.sh;
taskset -c 1 Reco_tf.py \
  --AMI q221 \
  --inputRDOFile /eos/user/s/sosen/RDO_CTIDE_timing/user.rjansky.mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.RDO_20180110_EXT0/user.rjansky.12944101.EXT0._005040.RDO.root \
  --maxEvents 10\
  --preInclude='CTIDENN_IDrecon_preInclude.py' \
  --preExec='from PerfMonComps.PerfMonFlags import jobproperties as pmjp;pmjp.PerfMonFlags.doPostProcessing=True;pmjp.PerfMonFlags.doSemiDetailedMonitoringFullPrint=True' \
  --outputAODFile myAOD.root 2>&1 | tee _log_localtestWithPerfMonSD_q221.txt;
  #--outputAODFile myAOD.root >& _log_localtestWithPerfMonSD_q221.txt;
